# BROWSERSTACK Changelog
---
- [# BROWSERSTACK Changelog](#h1-idbrowserstack-changelog-18browserstack-changelogh1)
- [Commit Details](#commit-details)
    - [Initial Commit](#initial-commit)
    - [BS-001](#bs-001)
    - [feature/navbar/BS-002](#featurenavbarbs-002)
    - [feature/navbar/BS-003](#featurenavbarbs-003)
    - [feature/home/BS-004](#featurehomebs-004)
    - [feature/home/BS-005](#featurehomebs-005)
    - [feature/home/BS-006](#featurehomebs-006)
    - [feature/footer/BS-006](#featurefooterbs-006)
    - [BS-007](#bs-007)
---

## Commit Details

#### Initial Commit

1. Initialized Angular App

[Back to top](#browserstack-changelog)

#### BS-001

1. Added `@angular/material` and `@angular/fortawesome`
2. Added module `bs-core`
3. Added components
   - `bs-auth`
   - `ba-customers`
   - `bs-footer`
   - `bs-home`
     - `bs-benefits`
     - `bs-intro`
     - `bs-stories`
     - `bs-testimonials`
   - `bs-navbar`
   - `bs-pricing`
     - `bs-plans`
     - `bs-questions`
   - `bs-error`
4. Setup application routes draft
   - Empty route redirects to `home` which displays home page
   - `auth` route displays auth page
   - `pricing` route redirects to pricing page

[Back to top](#browserstack-changelog)

#### feature/navbar/BS-002

1. Refactored components
   - At application level
     - `bs-app`
     - `bs-navbar`
     - `bs-footer`
   - At module level
     - `bs-error`
2. Routing setup complete
   - Everything works as per draft 4 in [BS-001](#bs-001)

[Back to top](#browserstack-changelog)

#### feature/navbar/BS-003

1. Navbar complete
   - Logo
   - Items iterated using `*ngFor`
   - Child items displayed on hover
   - Child items holder displays pointer
2. App components refactored

[Back to top](#browserstack-changelog)

#### feature/home/BS-004

1. Added `bs-intro` component
2. Added `bs-home` service to serve data into children of component `bs-home`
3. Standardized model names
4. Updated structure of `CHANGELOG.md`

[Back to top](#browserstack-changelog)

#### feature/home/BS-005

1. Added components
   - `bs-benefit`
   - `bs-stories`

[Back to top](#browserstack-changelog)

#### feature/home/BS-006

1. Added components
   - `bs-testimonials`

[Back to top](#browserstack-changelog)

#### feature/footer/BS-006

1. Added `bs-footer`

[Back to top](#browserstack-changelog)

#### BS-007

1. Updated `README.md`

[Back to top](#browserstack-changelog)
