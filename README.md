# BrowserStack-NG

[BrowserStack](https://www.browserstack.com/) homepage redesigned in [Angular 9](https://angular.io/)

## Prerequisites

1. NodeJS
2. NPM
3. Angular CLI

## Run the project

1. Clone the repository

```
git clone https://bitbucket.org/usama2519933/browserstack-ng.git
```

2. Install dependencies

```
npm install
```

3. Start the server

```
npm start
```

## Status

### Refer `CHANGELOG.md` for detailed status on every component

#### What's working

1. `browserstack-ng` home page
2. Smart and dumb component to display data
3. Observable based data exchange
4. Routing between components

#### What's not working

1. Homepage yet to be made responsive
2. `bs-auth` and `bs-pricing` components yet to be developed

## Troubleshooting

#### In case of failure of dependency installation

- Clean npm cache
- Upgrade `npm` to the latest version
- Upgrade global `@angular/cli` package

```
npm cache clean --force
npm i -g npm@latest
npm i -g @angular/cli@latest
```

#### Delete `package-lock.json` if below error is encountered

```
npm install integrity checksum failed when using sha512a
```