import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BsAppComponent } from './bs-app.component';

describe('BsAppComponent', () => {
  let component: BsAppComponent;
  let fixture: ComponentFixture<BsAppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BsAppComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BsAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
