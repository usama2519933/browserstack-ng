import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BsPlanComponent } from './bs-plan.component';

describe('BsPlanComponent', () => {
  let component: BsPlanComponent;
  let fixture: ComponentFixture<BsPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BsPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BsPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
