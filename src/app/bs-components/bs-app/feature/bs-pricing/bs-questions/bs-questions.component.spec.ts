import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BsQuestionsComponent } from './bs-questions.component';

describe('BsQuestionsComponent', () => {
  let component: BsQuestionsComponent;
  let fixture: ComponentFixture<BsQuestionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BsQuestionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BsQuestionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
