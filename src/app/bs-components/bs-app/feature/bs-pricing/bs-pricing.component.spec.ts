import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BsPricingComponent } from './bs-pricing.component';

describe('BsPricingComponent', () => {
  let component: BsPricingComponent;
  let fixture: ComponentFixture<BsPricingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BsPricingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BsPricingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
