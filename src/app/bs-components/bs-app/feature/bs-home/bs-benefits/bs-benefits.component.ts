import { Component, OnInit, Input } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { BsBenefitModel } from 'src/app/bs-models/bs-home.model';

@Component({
  selector: 'app-bs-benefits',
  templateUrl: './bs-benefits.component.html',
  styleUrls: ['./bs-benefits.component.scss']
})
export class BsBenefitsComponent implements OnInit {

  data$: BehaviorSubject<BsBenefitModel>

  @Input()
  set componentAssets(updatedValue: BsBenefitModel) { this.data$.next(updatedValue); };
  get componentAssets(): BsBenefitModel { return this.data$.getValue(); };

  constructor() {
    this.data$ = new BehaviorSubject<BsBenefitModel>(null);
  }

  ngOnInit(): void { }

}
