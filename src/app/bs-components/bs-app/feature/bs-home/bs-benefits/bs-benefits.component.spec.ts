import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BsBenefitsComponent } from './bs-benefits.component';

describe('BsBenefitsComponent', () => {
  let component: BsBenefitsComponent;
  let fixture: ComponentFixture<BsBenefitsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BsBenefitsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BsBenefitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
