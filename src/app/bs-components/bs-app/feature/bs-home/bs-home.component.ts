import { Component, OnInit } from '@angular/core';
import { BsHomeService, COMPONENT_STRINGS } from 'src/app/bs-services/bs-home.service';
import { Observable } from 'rxjs';
import { BsIntroModel, BsCustomerModel, BsBenefitModel, BsStoryModel, BsTestimonialModel } from 'src/app/bs-models/bs-home.model';

@Component({
  selector: 'app-bs-home',
  templateUrl: './bs-home.component.html',
  styleUrls: ['./bs-home.component.scss']
})
export class BsHomeComponent implements OnInit {

  introAssets$: Observable<BsIntroModel>;
  customerAssets$: Observable<BsCustomerModel>;
  benefitAssets$: Observable<BsBenefitModel>;
  storyAssets$: Observable<BsStoryModel>;
  testimonialAssets$: Observable<BsTestimonialModel>;

  constructor(private homeService: BsHomeService) { }

  ngOnInit(): void {
    this.introAssets$ = this.homeService.fetchComponentAssets(COMPONENT_STRINGS.INTRO);
    this.customerAssets$ = this.homeService.fetchComponentAssets(COMPONENT_STRINGS.CUSTOMERS);
    this.benefitAssets$ = this.homeService.fetchComponentAssets(COMPONENT_STRINGS.BENEFITS);
    this.storyAssets$ = this.homeService.fetchComponentAssets(COMPONENT_STRINGS.STORIES);
    this.testimonialAssets$ = this.homeService.fetchComponentAssets(COMPONENT_STRINGS.TESTIMONIALS);
  }

}
