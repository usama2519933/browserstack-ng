import { Component, OnInit, Input } from '@angular/core';
import { BsIntroModel } from 'src/app/bs-models/bs-home.model';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-bs-intro',
  templateUrl: './bs-intro.component.html',
  styleUrls: ['./bs-intro.component.scss']
})
export class BsIntroComponent implements OnInit {

  data$: BehaviorSubject<BsIntroModel>;

  @Input()
  set componentAssets(updatedValue: BsIntroModel) { this.data$.next(updatedValue); };
  get componentAssets(): BsIntroModel { return this.data$.getValue(); };

  constructor() {
    this.data$ = new BehaviorSubject<BsIntroModel>(null);
  }

  ngOnInit(): void {
  }

}
