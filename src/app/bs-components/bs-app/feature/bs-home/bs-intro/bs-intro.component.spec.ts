import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BsIntroComponent } from './bs-intro.component';

describe('BsIntroComponent', () => {
  let component: BsIntroComponent;
  let fixture: ComponentFixture<BsIntroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BsIntroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BsIntroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
