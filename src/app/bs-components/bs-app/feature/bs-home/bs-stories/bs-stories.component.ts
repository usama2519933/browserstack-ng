import { Component, OnInit, Input } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { BsStoryModel } from 'src/app/bs-models/bs-home.model';

@Component({
  selector: 'app-bs-stories',
  templateUrl: './bs-stories.component.html',
  styleUrls: ['./bs-stories.component.scss']
})
export class BsStoriesComponent implements OnInit {

  data$: BehaviorSubject<BsStoryModel>;

  @Input()
  set componentAssets(updatedValue: BsStoryModel) { this.data$.next(updatedValue); };
  get componentAssets(): BsStoryModel { return this.data$.getValue(); };

  constructor() {
    this.data$ = new BehaviorSubject<BsStoryModel>(null);
  }

  ngOnInit(): void { }

}
