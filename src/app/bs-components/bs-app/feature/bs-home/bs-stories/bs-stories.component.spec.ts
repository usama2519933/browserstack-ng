import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BsStoriesComponent } from './bs-stories.component';

describe('BsStoriesComponent', () => {
  let component: BsStoriesComponent;
  let fixture: ComponentFixture<BsStoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BsStoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BsStoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
