import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BsTestimonialsComponent } from './bs-testimonials.component';

describe('BsTestimonialsComponent', () => {
  let component: BsTestimonialsComponent;
  let fixture: ComponentFixture<BsTestimonialsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BsTestimonialsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BsTestimonialsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
