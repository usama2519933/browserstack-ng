import { Component, OnInit, Input } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { BsTestimonialModel } from 'src/app/bs-models/bs-home.model';

@Component({
  selector: 'app-bs-testimonials',
  templateUrl: './bs-testimonials.component.html',
  styleUrls: ['./bs-testimonials.component.scss']
})
export class BsTestimonialsComponent implements OnInit {

  data$: BehaviorSubject<BsTestimonialModel>;

  @Input()
  set componentAssets(updatedValue: BsTestimonialModel) { this.data$.next(updatedValue); };
  get componentAssets(): BsTestimonialModel { return this.data$.getValue(); };

  selectedTestimonial: BsTestimonialModel['testimonials'][0];

  constructor() {
    this.data$ = new BehaviorSubject<BsTestimonialModel>(null);
  }

  ngOnInit(): void {
    this.componentAssets.testimonials[0].bIsSelected = true;
    this.selectedTestimonial = this.componentAssets.testimonials[0];
  }

  viewStatement(testimonial: BsTestimonialModel['testimonials'][0]) {
    this.componentAssets.testimonials.forEach((singleTestimonial: BsTestimonialModel['testimonials'][0]) => {
      singleTestimonial.bIsSelected = false;
      if ([testimonial].includes(singleTestimonial)) {
        singleTestimonial.bIsSelected = true;
        this.selectedTestimonial = singleTestimonial;
      }
    });
  }

}
