import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BsAuthComponent } from './bs-auth.component';

describe('BsAuthComponent', () => {
  let component: BsAuthComponent;
  let fixture: ComponentFixture<BsAuthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BsAuthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BsAuthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
