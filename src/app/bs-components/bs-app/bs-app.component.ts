import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { BsNavbarModel } from 'src/app/bs-models/bs-navbar.model';
import { BsNavbarService } from 'src/app/bs-services/bs-navbar.service';
import { BsFooterModel } from 'src/app/bs-models/bs-footer.model';
import { BsFooterService } from 'src/app/bs-services/bs-footer.service';

@Component({
  selector: 'app-bs-app',
  templateUrl: './bs-app.component.html',
  styleUrls: ['./bs-app.component.scss']
})
export class BsAppComponent implements OnInit {

  navbarAssets$: Observable<BsNavbarModel>;
  footerAssets$: Observable<BsFooterModel>;

  constructor(
    private navbarService: BsNavbarService,
    private footerService: BsFooterService
  ) { }

  ngOnInit(): void {
    this.navbarAssets$ = this.navbarService.fetchComponentAssets();
    this.footerAssets$ = this.footerService.fetchComponentAssets();
  }

}
