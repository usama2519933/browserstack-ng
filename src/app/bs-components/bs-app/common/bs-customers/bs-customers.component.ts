import { Component, OnInit, Input } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { BsCustomerModel } from 'src/app/bs-models/bs-home.model';

@Component({
  selector: 'app-bs-customers',
  templateUrl: './bs-customers.component.html',
  styleUrls: ['./bs-customers.component.scss']
})
export class BsCustomersComponent implements OnInit {

  data$: BehaviorSubject<BsCustomerModel>;

  @Input()
  set componentAssets(updatedValue: BsCustomerModel) { this.data$.next(updatedValue); };
  get componentAssets(): BsCustomerModel { return this.data$.getValue(); };

  constructor() {
    this.data$ = new BehaviorSubject<BsCustomerModel>(null);
  }

  ngOnInit(): void { }

}
