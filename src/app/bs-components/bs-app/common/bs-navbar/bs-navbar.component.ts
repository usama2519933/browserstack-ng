import { Component, OnInit, Input } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { BsNavbarModel, BsNavbarItemModel } from 'src/app/bs-models/bs-navbar.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-bs-navbar',
  templateUrl: './bs-navbar.component.html',
  styleUrls: ['./bs-navbar.component.scss']
})
export class BsNavbarComponent implements OnInit {

  data$: BehaviorSubject<BsNavbarModel>;

  @Input()
  set componentAssets(updatedValue: BsNavbarModel) { this.data$.next(updatedValue); };
  get componentAssets(): BsNavbarModel { return this.data$.getValue(); };

  constructor(private router: Router) {
    this.data$ = new BehaviorSubject<BsNavbarModel>(null);
  }

  ngOnInit(): void {
  }

  navigate(item?: BsNavbarItemModel): void {
    (!!item)
      ? (!!item.navigation) ? this.router.navigate([item.navigation]) : void (0)
      : this.router.navigate(['home'])
  }
}
