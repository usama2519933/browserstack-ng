import { Component, OnInit, Input } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { BsFooterModel } from 'src/app/bs-models/bs-footer.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-bs-footer',
  templateUrl: './bs-footer.component.html',
  styleUrls: ['./bs-footer.component.scss']
})
export class BsFooterComponent implements OnInit {

  data$: BehaviorSubject<BsFooterModel>;

  @Input()
  set componentAssets(updatedValue: BsFooterModel) { this.data$.next(updatedValue); };
  get componentAssets(): BsFooterModel { return this.data$.getValue(); };

  constructor(private router: Router) {
    this.data$ = new BehaviorSubject<BsFooterModel>(null);
  }

  ngOnInit(): void { }

  navigate(): void {
    this.router.navigate(['home']);
  }

  toggleDoMore(): void {
    this.componentAssets.closure.doMore.bIsExpanded = !this.componentAssets.closure.doMore.bIsExpanded;
  }
}
