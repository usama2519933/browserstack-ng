import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BsAppComponent } from './bs-components/bs-app/bs-app.component';
import { BsHomeComponent } from './bs-components/bs-app/feature/bs-home/bs-home.component';
import { BsAuthComponent } from './bs-components/bs-app/feature/bs-auth/bs-auth.component';
import { BsPricingComponent } from './bs-components/bs-app/feature/bs-pricing/bs-pricing.component';


const routes: Routes = [
  {
    path: '', component: BsAppComponent,
    children: [
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      { path: 'home', component: BsHomeComponent },
      { path: 'auth', component: BsAuthComponent },
      { path: 'pricing', component: BsPricingComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
