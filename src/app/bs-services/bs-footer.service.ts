import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { BsFooterModel } from '../bs-models/bs-footer.model';

@Injectable({
  providedIn: 'root'
})
export class BsFooterService {

  footer: BsFooterModel;
  constructor() {
    this.footer = {
      logo: {
        icon: {
          style: 'fab',
          name: 'angular',
          transform: { size: '2x' }
        },
        label: 'BrowserStack'
      },
      sections: [
        {
          heading: 'Products',
          type: 'text',
          items: [
            { text: 'Live' },
            { text: 'Automate' },
            { text: 'App Live' },
            { text: 'App Automate' },
            { text: 'Screenshots' },
            { text: 'Responsive' },
            { text: 'Enterprise' },
            { text: 'SpeedLab ' }
          ]
        },
        {
          heading: 'Platforms',
          type: 'text',
          items: [
            { text: 'Browsers & Devices' },
            { text: 'Data Centers' },
            { text: 'Mobile Features' },
            { text: 'Security' }
          ]
        },
        {
          heading: 'Resources',
          type: 'text',
          items: [
            { text: 'Test on Right Devices' },
            { text: 'Support' },
            { text: 'Status' },
            { text: 'Release Notes' },
            { text: 'Case Studies' },
            { text: 'Blog' }
          ]
        },
        {
          heading: 'Company',
          type: 'text',
          items: [
            { text: 'About Us' },
            { text: 'Customers' },
            { text: 'Careers We’re hiring!' },
            { text: 'Open Source' },
            { text: 'Partners' },
            { text: 'Press' },
            { text: 'Contact' }
          ]
        },
        {
          heading: 'Social',
          type: 'icon',
          items: [
            {
              icon: {
                style: 'fab',
                name: 'twitter'
              }
            },
            {
              icon: {
                style: 'fab',
                name: 'facebook'
              }
            },
            {
              icon: {
                style: 'fab',
                name: 'linkedin'
              }
            },
            {
              icon: {
                style: 'fab',
                name: 'youtube'
              }
            },
            {
              icon: {
                style: 'fab',
                name: 'instagram'
              }
            }
          ]
        }
      ],
      closure: {
        doMore: {
          title: 'Do more with BrowserStack',
          bIsExpanded: false,
          links: [
            { label: 'Test In IE' },
            { label: 'Mobile Emulators' },
            { label: 'Test on iPhone' },
            { label: 'Test on iPad' },
            { label: 'Test on Galaxy' },
            { label: 'Android Testing' },
            { label: 'iOS Testing' },
            { label: 'Guide' },
            { label: 'Cross Browser Testing' },
            { label: 'Emulators & Simulators' },
            { label: 'Selenium' },
            { label: 'Android Emulators' },
            { label: 'Responsive Design' }
          ],
        },
        copyright: '© 2011-2020 BrowserStack - The Most Reliable Mobile App & Cross Browser Testing Company',
        policies: [
          { label: 'Terms of Service' },
          { label: 'Privacy Policy' },
          { label: 'Cookie Policy' },
          { label: 'Sitemap' }
        ]
      }
    }
  }

  fetchComponentAssets(): Observable<BsFooterModel> {
    return of(this.footer);
  }
}
