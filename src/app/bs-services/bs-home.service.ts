import { Injectable } from '@angular/core';
import { Observable, of, EMPTY } from 'rxjs';
import { BsIntroModel, BsCustomerModel, BsBenefitModel, BsStoryModel, BsTestimonialModel } from '../bs-models/bs-home.model';

export const COMPONENT_STRINGS = {
  INTRO: 'intro',
  CUSTOMERS: 'customers',
  BENEFITS: 'benefits',
  STORIES: 'stories',
  TESTIMONIALS: 'testimonials'
}

@Injectable({
  providedIn: 'root'
})
export class BsHomeService {

  introData: BsIntroModel;
  customerData: BsCustomerModel;
  benefitData: BsBenefitModel;
  storyData: BsStoryModel;
  testimonialData: BsTestimonialModel;

  constructor() {
    Object.freeze(COMPONENT_STRINGS);

    this.introData = {
      intro: {
        heading: 'App & Browser Testing Made Easy',
        subheadings: [
          'Give your users a seamless experience by testing on 2000+ real devices and browsers.',
          'Don\'t compromise with emulators and simulators.'
        ],
        button: 'Get Started Free'
      },
      holders: [
        {
          title: 'Test your websites',
          cards: [
            {
              title: 'LIVE',
              subtitle: 'Interactive cross browser testing',
              content: 'Image not available'
            },
            {
              title: 'AUTOMATE',
              subtitle: 'Instant Selenium testing',
              content: 'Image not available'
            }
          ]
        },
        {
          title: 'Test your mobile apps',
          cards: [
            {
              title: 'APP LIVE',
              subtitle: 'Interactive mobile app testing',
              content: 'Image not available'
            },
            {
              title: 'APP AUTOMATE',
              subtitle: 'Automated mobile app testing',
              content: 'Image not available'
            }
          ]
        }
      ]
    };
    this.customerData = {
      customers: [
        {
          icon: {
            name: 'microsoft',
            style: 'fab',
            transform: { size: '3x' }
          },
          label: 'Microsoft',
        },
        {
          icon: {
            name: 'js-square',
            style: 'fab',
            transform: { size: '3x' }
          },
          label: 'jQuery',
        },
        {
          icon: {
            name: 'twitter',
            style: 'fab',
            transform: { size: '3x' }
          },
          label: 'Twitter',
        },
        {
          icon: {
            name: 'compress-arrows-alt',
            style: 'fas',
            transform: { size: '3x' }
          },
          label: 'RBS',
        },
        {
          icon: {
            name: 'university',
            style: 'fas',
            transform: { size: '3x' }
          },
          label: 'Harvard',
        },
        {
          icon: {
            name: 'plane',
            style: 'fas',
            transform: { size: '3x' }
          },
          label: 'Expedia',
        },
        {
          icon: {
            name: 'wikipedia-w',
            style: 'fab',
            transform: { size: '3x' }
          },
          label: 'Wiki',
        }
      ],
      trust: 'Trusted by more than 25,000 customers globally'
    }
    this.benefitData = {
      title: 'Benefits',
      benefits: [
        {
          icons: [
            {
              style: 'fas',
              name: 'box-open',
              transform: { size: '4x' }
            }
          ],
          title: 'Works out of the box',
          subtitle: 'Zero setup and zero maintenance to speed up releases.'
        },
        {
          icons: [
            {
              style: 'fab',
              name: 'internet-explorer',
              transform: { size: 'lg' }
            },
            {
              style: 'fab',
              name: 'chrome',
              transform: { size: '3x' }
            },
            {
              style: 'fab',
              name: 'edge',
              transform: { size: '5x' }
            },
            {
              style: 'fab',
              name: 'firefox-browser',
              transform: { size: '3x' }
            },
            {
              style: 'fab',
              name: 'safari',
              transform: { size: 'lg' }
            }
          ],
          title: 'Comprehensive coverage',
          subtitle: 'Instant access to 2000+ browsers and real iOS and Android devices.'
        },
        {
          icons: [
            {
              style: 'fas',
              name: 'shield-alt',
              transform: { size: '4x' }
            }
          ],
          title: 'Uncompromising security',
          subtitle: 'SOC2 compliant. Pristine browsers and devices available for everyone, every time.'
        }
      ]
    }
    this.storyData = {
      title: 'Stories form our customers',
      stories: [
        {
          avatar: {
            style: 'fas',
            name: 'user'
          },
          logo: {
            style: 'fas',
            name: 'shopping-cart'
          },
          name: 'Kateryna Glushchuk',
          position: 'Senior Product Manager',
          experience: 'OLX delivers a seamless expeience for a localised globa audience'
        },
        {
          avatar: {
            style: 'fas',
            name: 'user'
          },
          logo: {
            style: 'fas',
            name: 'prescription'
          },
          name: 'Priyanka Halder',
          position: 'Senior Manager of Quality Engineering',
          experience: 'GoodRx cuts testing time by 90% to release 15 times a day'
        },
        {
          avatar: {
            style: 'fas',
            name: 'user'
          },
          logo: {
            style: 'fas',
            name: 'camera'
          },
          name: 'Martin Schneider',
          position: 'Delivery Manager',
          experience: 'Asia’s largest classified marketplace uses BrowserStack to scale automation'
        },
        {
          avatar: {
            style: 'fas',
            name: 'user'
          },
          logo: {
            style: 'far',
            name: 'circle'
          },
          name: 'Brian Lucas',
          position: 'Senior Staff Software Engineer',
          experience: 'Optimizely lowers developer pain by moving testing to the cloud'
        },
        {
          avatar: {
            style: 'fas',
            name: 'user'
          },
          logo: {
            style: 'fas',
            name: 'plane'
          },
          name: 'Kanak Kalburgi',
          position: 'Senior Automation Engineer',
          experience: 'Community marketplace Airtasker increases automation efficiency 12x'
        },
        {
          avatar: {
            style: 'fas',
            name: 'user'
          },
          logo: {
            style: 'fas',
            name: 'building'
          },
          name: 'Hyunoo Park',
          position: 'Full Stack Engineer',
          experience: 'Legal discovery platform Logikcull reduces test time by 73%'
        }
      ],
      actions: [
        {
          type: 'filled',
          text: 'View all Case Studies'
        },
        {
          type: 'stroked',
          text: 'See our Customers'
        }
      ]
    }
    this.testimonialData = {
      testimonials: [
        {
          statement: 'Just played around with BrowserStack: Quite cool, instant access to a browser in a VM with dev tools.',
          avatar: {
            style: 'fas',
            name: 'user',
            transform: { size: '4x' }
          },
          name: 'John Resig',
          info: 'Creator of jQuery'
        },
        {
          statement: 'Big thanks to @browserstack for letting me use their product for free to fix browser issues in React DnD.',
          avatar: {
            style: 'fas',
            name: 'user',
            transform: { size: '4x' }
          },
          name: 'Dan Abramov',
          info: 'React App'
        },
        {
          statement: '@BrowserStack is making moves by revamping their #opensource program! We have been testing with them and now can really make moves with some of their new initiatives.',
          avatar: {
            style: 'fas',
            name: 'user',
            transform: { size: '4x' }
          },
          name: 'Grommet',
          info: 'React UI Framework'
        }
      ],
      action: 'View Testimonials'
    };
  }

  fetchComponentAssets(component: string): Observable<any> {
    switch (component) {
      case COMPONENT_STRINGS.INTRO:
        return of(this.introData) as Observable<BsIntroModel>;
      case COMPONENT_STRINGS.CUSTOMERS:
        return of(this.customerData) as Observable<BsCustomerModel>;
      case COMPONENT_STRINGS.BENEFITS:
        return of(this.benefitData) as Observable<BsBenefitModel>;
      case COMPONENT_STRINGS.STORIES:
        return of(this.storyData) as Observable<BsStoryModel>;
      case COMPONENT_STRINGS.TESTIMONIALS:
        return of(this.testimonialData) as Observable<BsTestimonialModel>;
      default:
        return EMPTY;
    }
  }
}
