import { TestBed } from '@angular/core/testing';

import { BsHomeService } from './bs-home.service';

describe('BsHomeService', () => {
  let service: BsHomeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BsHomeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
