import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { BsNavbarModel } from '../bs-models/bs-navbar.model';


@Injectable({
  providedIn: 'root'
})
export class BsNavbarService {

  navbar: BsNavbarModel;

  constructor() {
    this.navbar = {
      logo: {
        icon: {
          style: 'fab',
          name: 'angular',
          transform: { size: '2x' }
        },
        label: 'BrowserStack'
      },
      items: [
        {
          label: 'Products',
          children: [
            { label: 'Live', children: [] },
            { label: 'Automate', children: [] }
          ]
        },
        {
          label: 'Developers',
          children: [
            { label: 'Documentation', children: [] },
            { label: 'Support', children: [] },
            { label: 'Status', children: [] },
            { label: 'Release Notes', children: [] },
            { label: 'Open Source', children: [] },
          ]
        },
        {
          label: 'Solutions',
          children: [
            { label: 'Functional Testing', children: [] },
            { label: 'Real User Condition Testing', children: [] },
            { label: 'Regression Testing', children: [] },
            { label: 'Geolocation Testing', children: [] },
            { label: 'Test in Local Environments', children: [] },
          ]
        },
        {
          label: 'Pricing',
          navigation: 'pricing',
          children: []
        },
        {
          label: 'Sign In',
          navigation: 'auth',
          children: []
        }
      ],
      expandIcon: {
        style: 'fas',
        name: 'angle-down'
      }
    }
  }

  fetchComponentAssets(): Observable<BsNavbarModel> {
    return of(this.navbar);
  }

}
