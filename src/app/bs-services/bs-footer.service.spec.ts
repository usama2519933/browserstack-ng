import { TestBed } from '@angular/core/testing';

import { BsFooterService } from './bs-footer.service';

describe('BsFooterService', () => {
  let service: BsFooterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BsFooterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
