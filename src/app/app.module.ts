import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { fas } from '@fortawesome/free-solid-svg-icons';

import { AppComponent } from './app.component';
import { BsAppComponent } from './bs-components/bs-app/bs-app.component';
import { BsMaterialModule } from './bs-modules/bs-material/bs-material.module';
import { BsCustomersComponent } from './bs-components/bs-app/common/bs-customers/bs-customers.component';
import { BsErrorComponent } from './bs-components/bs-app/common/bs-error/bs-error.component';
import { BsFooterComponent } from './bs-components/bs-app/common/bs-footer/bs-footer.component';
import { BsNavbarComponent } from './bs-components/bs-app/common/bs-navbar/bs-navbar.component';
import { BsAuthComponent } from './bs-components/bs-app/feature/bs-auth/bs-auth.component';
import { BsHomeComponent } from './bs-components/bs-app/feature/bs-home/bs-home.component';
import { BsPricingComponent } from './bs-components/bs-app/feature/bs-pricing/bs-pricing.component';
import { BsIntroComponent } from './bs-components/bs-app/feature/bs-home/bs-intro/bs-intro.component';
import { BsBenefitsComponent } from './bs-components/bs-app/feature/bs-home/bs-benefits/bs-benefits.component';
import { BsStoriesComponent } from './bs-components/bs-app/feature/bs-home/bs-stories/bs-stories.component';
import { BsTestimonialsComponent } from './bs-components/bs-app/feature/bs-home/bs-testimonials/bs-testimonials.component';
import { BsQuestionsComponent } from './bs-components/bs-app/feature/bs-pricing/bs-questions/bs-questions.component';
import { BsPlanComponent } from './bs-components/bs-app/feature/bs-pricing/bs-plan/bs-plan.component';

@NgModule({
  declarations: [
    AppComponent,
    BsAppComponent,
    BsCustomersComponent,
    BsErrorComponent,
    BsFooterComponent,
    BsNavbarComponent,
    BsAuthComponent,
    BsHomeComponent,
    BsPricingComponent,
    BsIntroComponent,
    BsBenefitsComponent,
    BsStoriesComponent,
    BsTestimonialsComponent,
    BsQuestionsComponent,
    BsPlanComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    BsMaterialModule,
    FontAwesomeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private lib: FaIconLibrary) {
    lib.addIconPacks(fab, far, fas)
  }
}
