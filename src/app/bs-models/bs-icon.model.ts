export interface BsFaIconModel {
  style: string;
  name: string;
  transform?: {
    size: string;
  };
}
