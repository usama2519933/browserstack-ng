import { BsFaIconModel } from './bs-icon.model';

export interface BsFooterModel {
  sections: {
    heading: string;
    type: 'text' | 'icon';
    items: {
      text?: string;
      icon?: BsFaIconModel;
    }[];
  }[];
  logo: {
    icon: BsFaIconModel;
    label: string;
  };
  closure?: {
    doMore: {
      title: string;
      bIsExpanded: boolean;
      links: {
        label: string
      }[];
    },
    copyright: string;
    policies: {
      label: string;
    }[]
  };
}
