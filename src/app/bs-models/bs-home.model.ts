import { BsFaIconModel } from './bs-icon.model';

export interface BsIntroModel {
  intro: {
    heading: string;
    subheadings: string[];
    button: string;
  };
  holders: {
    title: string;
    cards: {
      title: string;
      subtitle: string;
      content: string;
    }[];
  }[];
}

export interface BsCustomerModel {
  customers: {
    icon: BsFaIconModel;
    label: string;
  }[];
  trust: string;
}

export interface BsBenefitModel {
  title: string;
  benefits: {
    icons: BsFaIconModel[];
    title: string;
    subtitle: string;
  }[];
}

export interface BsStoryModel {
  title: string,
  stories: {
    avatar: BsFaIconModel;
    logo: BsFaIconModel;
    name: string;
    position: string;
    experience: string;
  }[];
  actions: {
    type: string;
    text: string;
  }[]
}

export interface BsTestimonialModel {
  testimonials: {
    statement: string;
    avatar: BsFaIconModel;
    name: string;
    info: string;
    bIsSelected?: boolean
  }[];
  action: string;
}
