import { BsFaIconModel } from './bs-icon.model';

export interface BsNavbarItemModel {
  label: string;
  bIsExpanded?: boolean;
  navigation?: string;
  children: BsNavbarItemModel[];
}

export interface BsNavbarModel {
  logo: {
    icon: BsFaIconModel;
    label: string;
  };
  items: BsNavbarItemModel[];
  expandIcon?: BsFaIconModel;
}
